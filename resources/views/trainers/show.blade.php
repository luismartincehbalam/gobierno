@extends('layouts.app')

@section('title', 'Treainer')

@include('common.success')
<img style="height:200px; width:200px; background-color:brown; " class="card-img-top rounded-circle mx-auto d-block" src="/images/{{$trainer->avatar }}" alt="">
<div class="text-center">
<h5 class="card-title">{{$trainer->name}}</h5>
<p >Some quick example text to build on the card title and make up the bulk of the card's content.
    I had a great weekend. On Friday afternoon, I finished work at 5 PM. I went home and took a shower. Then I went to see a couple of my friends at a bar downtown. 

We had a couple of beers and a nice talk. 

Tom told us about his new job, and Jim told us about his new girlfriend. After a while, we went to a restaurant and had pizza. I went to bed late that night, but I was very happy
</p>
<a href="/trainers/{{$trainer->slug}}/edit" class="btn btn-primary">Editar</a>
{!! Form::open(['route' => ['trainers.destroy', $trainer->slug], 'method' => 'DELETE']) !!}
{!! Form::submit('Eliminar', ['class'=>'btn btn-danger']) !!}
{!! Form::close() !!}
</div>
 
@endsection