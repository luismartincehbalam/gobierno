@extends('layouts.app')
@section('content')
<modal-button></modal-button>
<list-of-pokemons></list-of-pokemons>
<pokemons-component></pokemons-component>
@endsection