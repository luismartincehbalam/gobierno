<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('welcome');
});

Route::get('/prueba/{name}', 'pruebaController@prueba');

Route::get('/name/{name}/lastname/{lastname?}', function ($name, $lastname = 'apellido') {
    return 'soy nuevo '.$name . $lastname;
});

Route::resource('trainers', 'TrainerController');
Route::resource('pokemons', 'PokemonController');

Route::get('/foo', function () {
    return 'soy nuevo';
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
